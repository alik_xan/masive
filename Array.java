package Array;
public class Array {
  public static void main( String[] args ) {
    float[] b = {1333, 44, 565, 783, 673, 22, 46, 8};
    for ( int i = 0; i < b.length; i++ ) 
    {
      b[ i ] = ( float )( b[ i ] * 1.1 );
      for ( int j = b.length - 1; j > i; j-- )
        if ( b[ j ] > b[ j - 1 ] ) 
          swap( b, j, j - 1 );
    }
    for ( float n: b ) 
    {
      System.out.print( n + "; " );
    }
  }
  static void swap( float[] a, int left, int right ) {
    if ( left != right ) {
      float temp = a[ left ];
      a[ left ] = a[ right ];
      a[ right ] = temp;
    }
  }
}